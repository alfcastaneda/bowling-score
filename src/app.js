import './style.scss';
import { randomNum } from './utils';

import Game from './game';

const pinsEl = document.getElementById('pins');
const rollBall = document.getElementById('rollBall');
const scoreRows = document.querySelectorAll('.scores--row');

const game = new Game();

game.turns.forEach(turn => {
  const boxScore = document.createElement('div');
  boxScore.classList.add('score--box', `score--box--${turn.id}`);
  boxScore.innerHTML = `
      <div class="score--box-partials">
        <div class="score--box-partial"></div>
        <div class="score--box-partial"></div>
      </div>
      <div class="score--box-total"></div>
  `;
  scoreRows[0].appendChild(boxScore);
});

game.start(pinsEl);

pinsEl.innerHTML = game.TOTAL_PINS;
rollBall.addEventListener('click', event => {
  const droppedPins = randomNum({ max: game.availablePins });
  game.updateTurn(droppedPins);
  //game.updateTurnScore();
});
