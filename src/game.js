const TOTAL_PINS = 10;
const ROLLS_PER_TURN = 2;
const TOTAL_TURNS = 10;

export default class Game {
  constructor() {
    this.TOTAL_PINS = TOTAL_PINS;
    this.turns = Array.from(Array(TOTAL_TURNS)).map((value, index) => ({
      id: index + 1,
      rollsScores: [],
      availableRolls: ROLLS_PER_TURN,
      status: 'inactive'
    }));
  }

  start(pinsElement) {
    this.availablePins = TOTAL_PINS;
    this.currentTurnIndex = 0;
    this.pinsElement = pinsElement;
    this.pinsElement.innerHTML = this.availablePins;
    this.strikeArray = [];
    this.spareArray = [];
  }

  updatePins() {
    pins.innerHTML = this.availablePins;
  }

  updateTurn(droppedPins) {
    if (this.currentTurnIndex < TOTAL_TURNS) {
      this.availablePins = this.availablePins - droppedPins;
      let { status, availableRolls, rollsScores, score } = this.turns[
        this.currentTurnIndex
      ];
      rollsScores.push(droppedPins);
      if (droppedPins === TOTAL_PINS) {
        status = 'strike';
        score = 10;
        availableRolls = 0;
      } else {
        score = rollsScores.reduce((value, acc) => value + acc, 0);
        status =
          score === TOTAL_PINS
            ? 'spare'
            : availableRolls > 0 ? 'in_progess' : 'done';
        availableRolls--;
      }
      this.turns[this.currentTurnIndex] = {
        ...this.turns[this.currentTurnIndex],
        status,
        availableRolls,
        rollsScores,
        score
      };
      // console.log(this.turns[this.currentTurnIndex]);
      this.updateTurnScore(rollsScores, status);
      if (availableRolls == 0) {
        this.currentTurnIndex++;
        this.availablePins = TOTAL_PINS;
      }
    } else {
      pins.innerHTML = `You reached the end of the game`;
      console.log('END GAME');
    }
    this.updatePins();
  }

  updateTurnScore(rollsScores, status, availableRolls) {
    const currentBox = document.querySelectorAll('.score--box-partials')[
      this.currentTurnIndex
    ];
    const partials = currentBox.querySelectorAll('.score--box-partial');
    const rolls = rollsScores.length;
    const turnScore = rollsScores.reduce((value, acc) => value + acc, 0);
    if (status === 'strike') {
      partials[0].innerHTML = 'X';
    } else {
      partials[rolls - 1].innerHTML =
        rollsScores[rolls - 1] === 0
          ? '-'
          : turnScore === TOTAL_PINS ? '/' : rollsScores[rolls - 1];
    }

    this.calculateScore();
  }

  calculateScore() {
    const passedTurns = this.turns.filter(
      turn => turn.score !== undefined && turn.availableRolls === 0
    );
    const partialScores = passedTurns.map((turn, index) => {
      if (turn.status === 'strike') {
        let newValue = 0;
        if (
          passedTurns[index + 1] &&
          passedTurns[index + 1].status !== 'strike'
        ) {
          newValue = passedTurns[index + 1].score;
        } else if (passedTurns[index + 2]) {
          newValue =
            passedTurns[index + 1].score +
            passedTurns[index + 2].rollsScores[0];
        }
        turn.score = 10 + newValue;
      } else if (turn.status === 'spare' && passedTurns[index + 1]) {
        turn.score = 10 + passedTurns[index + 1].rollsScores[0];
      }
      return turn.score;
    });
    if (partialScores.length) {
      const totalScore = partialScores.reduce((value, acc) => value + acc);
      if (this.turns[this.currentTurnIndex].availableRolls === 0) {
        console.log(partialScores);
        this.updateDOMBoxes(totalScore);
      }
    }
  }

  updateDOMBoxes(totalScore) {
    const boxes = document.querySelectorAll('.score--box');
    const turnScoreBox = boxes[this.currentTurnIndex].querySelector(
      '.score--box-total'
    );
    turnScoreBox.innerHTML = totalScore;
  }
}
