const randomNum = ({ min = 0, max = 10 } = {}) =>
  Math.round(Math.random() * (max - min) + min);

export { randomNum };
